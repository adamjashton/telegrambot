﻿namespace TelegramBot
{
    public static class Constants
    {
        public const string TelegramIncomingWebHookUriWithToken = "api/telegram-incoming-webhook/{token}";
    }
}

﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using TelegramBot.TelegramApi;

namespace TelegramBot.CommandHandlers
{
    public class CommandHandlerManager
    {
        public static HashSet<Type> ListHandlers { get; private set; }
            = new HashSet<Type>();

        private IServiceProvider serviceProvider;
        private readonly ILogger<CommandHandlerManager> logger;

        public CommandHandlerManager(IServiceProvider serviceProvider, ILogger<CommandHandlerManager> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public bool TryFindHandler(Message message, out ICommandHandler commandHandler)
        {
            if (WillHandle(message))
            {
                foreach (Type type in ListHandlers)
                {
                    var handler = (ICommandHandler)serviceProvider.GetService(type);
                    if (handler.CanHandle(message))
                    {
                        commandHandler = handler;
                        return true;
                    }
                }
            }
            else
            {
                logger.LogInformation($"Will not handle: ignoring message from {message.Chat.Id} with Text '{message.Text}'");
            }

            commandHandler = null;
            return false;
        }

        public bool WillHandle(Message message)
        {
            bool result = !message.From.IsBot;

            result &= string.Equals(message.Chat.Type, "private", StringComparison.OrdinalIgnoreCase)
                || string.Equals(message.Chat.Type, "group", StringComparison.OrdinalIgnoreCase)
                || string.Equals(message.Chat.Type, "supergroup", StringComparison.OrdinalIgnoreCase);

            result &= !string.IsNullOrWhiteSpace(message.Text);

            result &= MessageIsRecent(message.Date);

            return result;
        }

        private bool MessageIsRecent(long date)
        {
            TimeSpan recentMessageThreshold = TimeSpan.FromMinutes(30);
            var messageTimestamp = DateTimeOffset.FromUnixTimeSeconds(date).ToUniversalTime();
            var now = DateTimeOffset.Now.ToUniversalTime();
            TimeSpan messageAge = now.Subtract(messageTimestamp);
            return messageAge < recentMessageThreshold;
        }
    }
}

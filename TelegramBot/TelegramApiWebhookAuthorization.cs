﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using TelegramBot.TelegramApi;

namespace TelegramBot
{
    public class TelegramApiWebhookAuthorization : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly ILogger<TelegramApiWebhookAuthorization> logger;
        private readonly TelegramWebhookTokenStore telegramWebhookTokenStore;
        
        public TelegramApiWebhookAuthorization(ILogger<TelegramApiWebhookAuthorization> logger, 
            TelegramWebhookTokenStore telegramWebhookTokenStore)
        {
            this.logger = logger;
            this.telegramWebhookTokenStore = telegramWebhookTokenStore;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string requestPath = context.HttpContext.Request.Path;
            string webhookToken = telegramWebhookTokenStore.GetToken();

            if (string.IsNullOrEmpty(webhookToken))
            {
                logger.LogError($"Received request with invalid token: {context.HttpContext.Request.Method} {context.HttpContext.Request.Path}");
                context.Result = new UnauthorizedResult();
            }   

            if (!requestPath.Contains(webhookToken, StringComparison.OrdinalIgnoreCase))
            {
                logger.LogError($"Received request with invalid token. {context.HttpContext.Request.Method} {context.HttpContext.Request.Path}");
                context.Result = new UnauthorizedResult();
            }
        }
    }
}

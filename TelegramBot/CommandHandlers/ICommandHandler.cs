﻿using System.Threading.Tasks;
using TelegramBot.TelegramApi;

namespace TelegramBot.CommandHandlers
{
    public interface ICommandHandler
    {
        bool CanHandle(Message message);

        Task Handle(Message message);
    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using TelegramBot.CommandHandlers;
using TelegramBot.ImageSearch;
using TelegramBot.TelegramApi;

namespace TelegramBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.WriteIndented = true;
                });

            services.AddSingleton(Configuration);


            // Telegram
            services.AddSingleton<TelegramWebhookTokenStore>();
            services.AddScoped<TelegramApi.Client>();
            services.AddScoped<TelegramWebhookConfigurator>();
            services.AddScoped<CommandHandlerManager>();

            // Bing Image Search
            services.AddSingleton<ImageClientWaiter>();
            services.AddScoped<ImageSearch.Client>();

            // Register handlers            
            services.RegisterHandler<ImageCommandHandler>();
            services.RegisterHandler<LadsCommandHandler>();
            services.RegisterHandler<NucoRepsCommandHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    public static class ServiceProviderExtensions
    {
        public static void RegisterHandler<TService>(this IServiceCollection services) where TService : ICommandHandler
        {
            services.AddScoped(typeof(TService));
            CommandHandlerManager.ListHandlers.Add(typeof(TService));
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.ImageSearch;
using TelegramBot.TelegramApi;

namespace TelegramBot.CommandHandlers
{
    public class ImageCommandHandler : ICommandHandler
    {
        private readonly Random random = new Random();
        private readonly ImageSearch.Client imageSearchClient;
        private readonly TelegramApi.Client telegramClient;
        private readonly ImageClientWaiter imageClientWaiter;
        private readonly ILogger<ImageCommandHandler> logger;

        public ImageCommandHandler(ImageSearch.Client imageSearchClient,
            TelegramApi.Client telegramClient,
            ImageClientWaiter imageClientWaiter,
            ILogger<ImageCommandHandler> logger)
        {
            this.imageSearchClient = imageSearchClient;
            this.telegramClient = telegramClient;
            this.imageClientWaiter = imageClientWaiter;
            this.logger = logger;
        }

        public bool CanHandle(Message message)
        {
            return message.Text.ToLower().StartsWith("/image");
        }

        public async Task Handle(Message message)
        {
            if (!CanHandle(message))
                throw new NotImplementedException("Cannot handle message.");

            string searchText = message.Text
                .ToLower()
                .Replace("/image", string.Empty)
                .Replace("  ", " ")
                .Trim();

            if (string.IsNullOrWhiteSpace(searchText))
                return; // ignore

            if (imageClientWaiter.TryProceed())
            {
                Uri imageUrl = await FindImageAsync(searchText);
                await SendImageToChatAsync(message, imageUrl);
            }
            else
            {
                // spammy
                await SendWaitMessageAsync(message);
            }
        }

        private async Task SendWaitMessageAsync(Message message)
        {
            await telegramClient.SendMessage(message.Chat.Id, $"Too many requests, ignoring {message.Text}");
        }

        private async Task SendImageToChatAsync(Message message, Uri imageUrl)
        {
            await telegramClient.SendPhoto(message.Chat.Id, imageUrl);
        }

        private async Task<Uri> FindImageAsync(string searchText)
        {
            // clean search text
            string searchTextClean = ConvertToAscii(searchText);

            logger.LogInformation($"Performing image search: '{searchTextClean}'");
            ImageSearchResponse imageSearchResponse = await imageSearchClient.SearchAsync(searchTextClean);

            if (imageSearchResponse == null
                || imageSearchResponse.Value == null
                || imageSearchResponse.Value.Count == 0)
                throw new Exception($"No images returned from Image Search for search text: {searchText}");

            // Find a random one
            int index = this.random.Next(0, imageSearchResponse.Value.Count);
            string imageUrl = imageSearchResponse.Value[index].ContentUrl;
            bool canDownloadImage = await CanDownloadImage(imageUrl);

            if (!canDownloadImage)
            {
                // try again
                index = this.random.Next(0, imageSearchResponse.Value.Count);
                imageUrl = imageSearchResponse.Value[index].ContentUrl;
                canDownloadImage = await CanDownloadImage(imageUrl);
            }

            if (!canDownloadImage)
            {
                throw new Exception($"Could not download an image for search text: {searchText}");
            }

            return new Uri(imageUrl, UriKind.Absolute);
        }

        private async Task<bool> CanDownloadImage(string imageUrl)
        {
            bool isValid = Uri.TryCreate(imageUrl, UriKind.Absolute, out Uri uri);

            if (!isValid)
                return false;

            using (var httpClient = new HttpClient())
            {
                var uaHeader = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36";
                httpClient.DefaultRequestHeaders.UserAgent.ParseAdd(uaHeader);

                try
                {
                    var imageResponse = await httpClient.GetAsync(imageUrl);
                    imageResponse.EnsureSuccessStatusCode();
                    return true;
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Failed to download image from address: {imageUrl}", imageUrl);
                    return false;
                }
            }
        }

        private string ConvertToAscii(string s)
        {
            byte[] asciiTextAsBytes = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(string.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s));
            string result = Encoding.ASCII.GetString(asciiTextAsBytes);
            return result;
        }
    }
}

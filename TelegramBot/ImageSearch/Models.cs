﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TelegramBot.ImageSearch
{
    public class Instrumentation
    {
        [JsonPropertyName("_type")]
        public string Type { get; set; }
    }

    public class QueryContext
    {
        [JsonPropertyName("originalQuery")]
        public string OriginalQuery { get; set; }

        [JsonPropertyName("alterationDisplayQuery")]
        public string AlterationDisplayQuery { get; set; }

        [JsonPropertyName("alterationOverrideQuery")]
        public string AlterationOverrideQuery { get; set; }

        [JsonPropertyName("alterationMethod")]
        public string AlterationMethod { get; set; }

        [JsonPropertyName("alterationType")]
        public string AlterationType { get; set; }
    }

    public class Thumbnail
    {
        [JsonPropertyName("width")]
        public int Width { get; set; }

        [JsonPropertyName("height")]
        public int Height { get; set; }
    }

    public class InsightsMetadata
    {
        [JsonPropertyName("pagesIncludingCount")]
        public int PagesIncludingCount { get; set; }

        [JsonPropertyName("availableSizesCount")]
        public int AvailableSizesCount { get; set; }
    }

    public class Value
    {
        [JsonPropertyName("webSearchUrl")]
        public string WebSearchUrl { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("thumbnailUrl")]
        public string ThumbnailUrl { get; set; }

        [JsonPropertyName("datePublished")]
        public DateTime DatePublished { get; set; }

        [JsonPropertyName("isFamilyFriendly")]
        public bool IsFamilyFriendly { get; set; }

        [JsonPropertyName("contentUrl")]
        public string ContentUrl { get; set; }

        [JsonPropertyName("hostPageUrl")]
        public string HostPageUrl { get; set; }

        [JsonPropertyName("contentSize")]
        public string ContentSize { get; set; }

        [JsonPropertyName("encodingFormat")]
        public string EncodingFormat { get; set; }

        [JsonPropertyName("hostPageDisplayUrl")]
        public string HostPageDisplayUrl { get; set; }

        [JsonPropertyName("width")]
        public int Width { get; set; }

        [JsonPropertyName("height")]
        public int Height { get; set; }

        [JsonPropertyName("thumbnail")]
        public Thumbnail Thumbnail { get; set; }

        [JsonPropertyName("imageInsightsToken")]
        public string ImageInsightsToken { get; set; }

        [JsonPropertyName("insightsMetadata")]
        public InsightsMetadata InsightsMetadata { get; set; }

        [JsonPropertyName("imageId")]
        public string ImageId { get; set; }

        [JsonPropertyName("accentColor")]
        public string AccentColor { get; set; }

        [JsonPropertyName("creativeCommons")]
        public string CreativeCommons { get; set; }

        [JsonPropertyName("hostPageFavIconUrl")]
        public string HostPageFavIconUrl { get; set; }

        [JsonPropertyName("hostPageDomainFriendlyName")]
        public string HostPageDomainFriendlyName { get; set; }
    }

    public class PivotSuggestion
    {
        [JsonPropertyName("pivot")]
        public string Pivot { get; set; }

        [JsonPropertyName("suggestions")]
        public List<object> Suggestions { get; set; }
    }

    public class ImageSearchResponse
    {
        [JsonPropertyName("_type")]
        public string Type { get; set; }

        [JsonPropertyName("instrumentation")]
        public Instrumentation Instrumentation { get; set; }

        [JsonPropertyName("readLink")]
        public string ReadLink { get; set; }

        [JsonPropertyName("webSearchUrl")]
        public string WebSearchUrl { get; set; }

        [JsonPropertyName("queryContext")]
        public QueryContext QueryContext { get; set; }

        [JsonPropertyName("totalEstimatedMatches")]
        public int TotalEstimatedMatches { get; set; }

        [JsonPropertyName("nextOffset")]
        public int NextOffset { get; set; }

        [JsonPropertyName("currentOffset")]
        public int CurrentOffset { get; set; }

        [JsonPropertyName("value")]
        public List<Value> Value { get; set; }

        [JsonPropertyName("pivotSuggestions")]
        public List<PivotSuggestion> PivotSuggestions { get; set; }
    }


}

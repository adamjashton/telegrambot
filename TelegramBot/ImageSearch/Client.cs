﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace TelegramBot.ImageSearch
{
    public class Client
    {
        private readonly Uri baseUrl;
        private readonly string apiToken;

        public Client(IConfiguration configuration)
        {
            this.baseUrl = new Uri(configuration.GetValue<string>("azure:endpoint"));
            this.apiToken = configuration.GetValue<string>("azure:api-key");
        }

        public async Task<ImageSearchResponse> SearchAsync(string searchText)
        {
            const string method = "/v7.0/images/search";

            string urlEncodedSearchText = System.Web.HttpUtility.UrlEncode(searchText);
            string queryParams = $"?q={urlEncodedSearchText}&safeSearch=Off";

            Uri requestUri = new Uri($"{method}{queryParams}", UriKind.Relative);

            ImageSearchResponse imageSearchReponse = null;
            using (var httpClient = CreateHttpClient())
            {
                var result = await httpClient.GetAsync(requestUri, HttpCompletionOption.ResponseContentRead);
                result.EnsureSuccessStatusCode();
                var jsonResult = await result.Content.ReadAsStringAsync();
                imageSearchReponse = JsonSerializer.Deserialize<ImageSearchResponse>(jsonResult);
            }

            return imageSearchReponse;
        }

        private HttpClient CreateHttpClient()
        {
            HttpClient result = new HttpClient
            {
                BaseAddress = this.baseUrl
            };
            result.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this.apiToken);
            return result;
        }
    }
}

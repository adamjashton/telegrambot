Write-Host "Stopping Website..."
New-Item -ItemType file -Force C:\websites\telegrambot\bin\App_Offline.htm
Stop-Website -Name TelegramBot
Start-Sleep -Seconds 3.0
Write-Host "Website Stopped."

Write-Host "Removing old content."
Remove-Item -path 'C:\websites\telegrambot\bin\*' -Recurse -Force -EA SilentlyContinue -Verbose

Write-Host "Publishing new site."
dotnet publish --output C:\websites\telegrambot\bin --configuration Release --no-self-contained .\TelegramBot\TelegramBot.csproj
Copy-Item -Path "C:\secure\telegrambot\*" -Destination "C:\websites\telegrambot\bin" -Recurse -Force -EA SilentlyContinue -Verbose

Write-Host "Starting Website..."
Start-Website TelegramBot
Start-Sleep -Seconds 3.0
Invoke-WebRequest -URI https://telegrambot.adamashton.com/api/health/check -UseBasicParsing
Write-Host "Started Website."

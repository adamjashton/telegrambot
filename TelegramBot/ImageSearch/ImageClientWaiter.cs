﻿using System;

namespace TelegramBot.ImageSearch
{
    public class ImageClientWaiter
    {
        private readonly TimeSpan delay = TimeSpan.FromSeconds(10);
        private readonly object locker = new object();
        
        private DateTime lastUsed = DateTime.MinValue;

        public bool TryProceed()
        {
            lock (locker)
            {
                DateTime utcNow = DateTime.UtcNow;
                TimeSpan timeSinceUsed = utcNow.Subtract(lastUsed);
                if (timeSinceUsed > delay)
                {
                    // allow
                    lastUsed = utcNow;
                    return true;
                }

                return false;
            }
        }
    }
}

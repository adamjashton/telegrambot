﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using TelegramBot.TelegramApi;

namespace TelegramBot.CommandHandlers
{
    public class NucoRepsCommandHandler : ICommandHandler
    {
        private readonly IConfiguration configuration;
        private readonly Client client;

        public NucoRepsCommandHandler(IConfiguration configuration, Client client)
        {
            this.configuration = configuration;
            this.client = client;
        }

        public bool CanHandle(Message message)
        {
            return message.Text.ToLower().Contains("calling all nuco reps");
        }

        public async Task Handle(Message message)
        {
            string ladsCommandText = configuration.GetValue<string>("lads-command:text").ToLower();
            string response = $"{ladsCommandText} {ladsCommandText} {ladsCommandText} !";
            await client.SendMessage(message.Chat.Id, response);
        }
    }
}

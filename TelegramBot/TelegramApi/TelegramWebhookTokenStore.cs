﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Text;

namespace TelegramBot.TelegramApi
{
    public class TelegramWebhookTokenStore
    {
        private static object _locker = new object();
        private const string filename = @"C:\secure\telegrambot\webhook-token-store.txt";
        private readonly ILogger<TelegramWebhookTokenStore> logger;
        private string _token;

        public TelegramWebhookTokenStore(ILogger<TelegramWebhookTokenStore> logger)
        {
            this.logger = logger;            
        }

        public string GetToken()
        {
            lock (_locker)
            {
                if (!string.IsNullOrEmpty(_token))
                    return _token;

                if (TryLoadFromFile())
                    return _token;

                throw new InvalidOperationException("Token has not been generated.");
            }
        }

        public void RemoveToken()
        {
            logger.LogInformation("Removing token.");

            lock (_locker)
            {
                DeleteFile();
                _token = null;
            }
        }

        /// <summary>Returns TRUE if had to generate new token</summary>
        public bool EnsureTokenExists()
        {
            lock (_locker)
            {
                // If we have one saved, do nothing
                if (!string.IsNullOrEmpty(_token))
                    return false;

                // Attempt to load from file
                bool loadedToken = TryLoadFromFile();
                if (loadedToken)
                {
                    logger.LogInformation("Loaded token from file.");
                    return false;
                }   

                // Generate new one and save
                logger.LogInformation("Generating new token.");
                _token = Guid.NewGuid().ToString("N");
                File.WriteAllText(filename, _token, Encoding.UTF8);
                return true;
            }
        }

        private bool TryLoadFromFile()
        {
            bool loadedToken = false;
            try
            {
                if (File.Exists(filename))
                {
                    _token = File.ReadAllText(filename, Encoding.UTF8);
                    loadedToken = !string.IsNullOrEmpty(_token);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Failed to load token from file: {filename}");
            }

            return loadedToken;
        }

        private void DeleteFile()
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Failed to load token from file: {filename}");
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

namespace TelegramBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        [HttpGet("check")]
        public IActionResult Check()
        {
            
            return new JsonResult(new { Message = "Ok" });
        }
    }
}

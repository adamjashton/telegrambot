﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TelegramBot.TelegramApi;

namespace TelegramBot.CommandHandlers
{
    public class LadsCommandHandler : ICommandHandler
    {
        private readonly ImageCommandHandler imageCommandHandler;
        private readonly Client telegramClient;
        private readonly string command;

        public LadsCommandHandler(IConfiguration configuration,
            ImageCommandHandler imageCommandHandler,
            Client telegramClient)
        {
            this.imageCommandHandler = imageCommandHandler;
            this.telegramClient = telegramClient;
            this.command = configuration.GetValue<string>("lads-command:text").ToLower();
        }

        public bool CanHandle(Message message)
        {
            string fullCommand = $"/{command} ";
            return message.Text.ToLower().StartsWith(fullCommand);
        }

        public async Task Handle(Message message)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                await SendSundayMessage(message);
                return;
            }

            string commandWithoutSlash = message.Text.Replace("/", string.Empty);
            string newCommand = $"/image {commandWithoutSlash}";
            message.Text = newCommand;
            await imageCommandHandler.Handle(message);
        }

        private async Task SendSundayMessage(Message message)
        {
            await telegramClient.SendMessage(message.Chat.Id, $"Sunday is the day of rest ... from {command}");
        }
    }
}

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog.Web;
using System;
using System.Threading.Tasks;

namespace TelegramBot
{
    public class Program
    {
        private static IHost host;

        public static void Main(string[] args)
        {
            var bootupLogger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                host = CreateHostBuilder(args).Build();
                ConfigureWebHook(host);
                host.Run();
            }
            catch (Exception e)
            {
                bootupLogger.Error(e, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, config) =>
                {
                    config.AddJsonFile("secure.json", optional: true, reloadOnChange: true);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseNLog();

        public static IServiceProvider ServiceProvider { get => host.Services; }

        private static void ConfigureWebHook(IHost host)
        {
            var serviceScopeFactory = host.Services.GetService<IServiceScopeFactory>();
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var webhookConfigurator = (TelegramWebhookConfigurator)scope.ServiceProvider.GetService(typeof(TelegramWebhookConfigurator));
                Task task = webhookConfigurator.ConfigureWebHook(generateNewToken: false, notifyTelegramAboutToken: true);
                task.ConfigureAwait(false).GetAwaiter().GetResult();
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.CommandHandlers;
using TelegramBot.TelegramApi;

namespace TelegramBot.Controllers
{
    [ApiController]
    public class TelegramIncomingWebHookController : ControllerBase
    {
        private readonly CommandHandlerManager commandHandlerManager;
        private readonly ILogger<TelegramIncomingWebHookController> logger;

        public TelegramIncomingWebHookController(
            CommandHandlerManager commandHandlerManager,
            ILogger<TelegramIncomingWebHookController> logger)
        {
            this.commandHandlerManager = commandHandlerManager;
            this.logger = logger;
        }

        [TypeFilter(typeof(TelegramApiWebhookAuthorization))]
        [Route(Constants.TelegramIncomingWebHookUriWithToken)]
        public async Task<IActionResult> OnPostAsync([FromBody] WebhookUpdate webhookUpdate)
        {
            if (webhookUpdate == null)
            {
                logger.LogWarning("Webhook was either missing or could not be parsed");
                LogHttpRequest();
                return Ok();
            }

            if (webhookUpdate.OriginalMessage == null && webhookUpdate.EditedMessage != null)
            {
                logger.LogInformation("Received edited message, ignoring.");
                return Ok();
            }

            logger.LogDebug($"Received Web hook update with text: {webhookUpdate.OriginalMessage.Text}");

            if (commandHandlerManager.TryFindHandler(webhookUpdate.OriginalMessage, out ICommandHandler commandHandler))
            {
                try
                {
                    logger.LogInformation($"{commandHandler.GetType().Name} handling {webhookUpdate.OriginalMessage.Text}");
                    await commandHandler.Handle(webhookUpdate.OriginalMessage);
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Could not handle message {message}", webhookUpdate.OriginalMessage);
                }
            }

            return Ok();
        }

        private void LogHttpRequest()
        {
            try
            {
                var buffer = new byte[Convert.ToInt32(HttpContext.Request.ContentLength)];
                HttpContext.Request.Body.ReadAsync(buffer, 0, buffer.Length);
                var requestContent = Encoding.UTF8.GetString(buffer);
                logger.LogInformation($"Request content body was: {requestContent}");
            }
            catch (Exception e)
            {
                logger.LogError(e, "Could not read request content body.");
            }
            
        }
    }
}

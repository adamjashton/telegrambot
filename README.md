# Telegram Image Bot
On startup webserver will set a webhook with Telegram that ends with a new random guid to enable updates to be sent to the server.

## Running
1. Configure `appsettings.json` or `appsettings.Development.json` if running in Development
Note: Telegram webhook will only work with an SSL URL
2. Create file `secure.json` that will exist in the bin folder
```
{
  "azure": {
    "api-key": "azure-api-key"
  },
  "telegram": {
    "token": "telegram-bot-token"
  }
}
```
3. Run with `dotnet run` or Visual Studio.
4. Visit `/api/start` 

## Using
1. Bot will respond to `/image search text` and return image in private message or group chats.

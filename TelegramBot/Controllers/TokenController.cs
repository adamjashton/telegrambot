﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TelegramBot.TelegramApi;

namespace TelegramBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : Controller
    {
        private readonly TelegramWebhookConfigurator telegramWebhookConfigurator;

        public TokenController(TelegramWebhookConfigurator telegramWebhookConfigurator)
        {
            this.telegramWebhookConfigurator = telegramWebhookConfigurator;
        }

        [HttpPost("Renew")]
        public async Task<IActionResult> Generate()
        {
            await telegramWebhookConfigurator.ConfigureWebHook(generateNewToken: true, notifyTelegramAboutToken: true);
            return Ok();
        }
    }
}

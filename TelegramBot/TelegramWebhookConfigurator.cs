﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TelegramBot.TelegramApi;

namespace TelegramBot
{
    public class TelegramWebhookConfigurator
    {
        private readonly ILogger<TelegramWebhookConfigurator> logger;
        private readonly IConfiguration configuration;
        private readonly Client client;
        private readonly TelegramWebhookTokenStore telegramWebhookTokenStore;

        public TelegramWebhookConfigurator(
            ILogger<TelegramWebhookConfigurator> logger,
            IConfiguration configuration,
            Client client, 
            TelegramWebhookTokenStore telegramWebhookTokenStore)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.client = client;
            this.telegramWebhookTokenStore = telegramWebhookTokenStore;
        }

        public async Task ConfigureWebHook(bool generateNewToken, bool notifyTelegramAboutToken)
        {
            if (generateNewToken && !notifyTelegramAboutToken)
            {
                throw new InvalidOperationException("Cannot generate new token and not notify Telegram.");
            }

            if (generateNewToken)
            {
                telegramWebhookTokenStore.RemoveToken();
            }

            bool generatedNewToken = telegramWebhookTokenStore.EnsureTokenExists();

            if (generatedNewToken || notifyTelegramAboutToken)
            {
                if (configuration.GetValue<bool>("webhook:pipedream:enabled"))
                {
                    logger.LogInformation("Configuring Telegram webhook for Pipedream");
                    await ConfigureForPipedream();
                }
                else
                {
                    logger.LogInformation("Configuring Telegram webook server for this server");
                    await ConfigureForThisServer(telegramWebhookTokenStore.GetToken());
                }
            }
        }

        private async Task ConfigureForThisServer(string webHookToken)
        {
            string host = configuration.GetValue<string>("webhook:host");            
            string webHookUrl = $"{host}/{Constants.TelegramIncomingWebHookUriWithToken}";
            webHookUrl = webHookUrl.Replace("{token}", webHookToken);
            Uri address = new Uri(webHookUrl, UriKind.Absolute);
            await client.SetWebHook(address);

            // This is silly but sometimes telegram doesn't listen - so tell em again!
            await Task.Delay(1200);
            await client.SetWebHook(address);
        }

        private async Task ConfigureForPipedream()
        {
            Uri uri = new Uri(configuration.GetValue<string>("webhook:pipedream:url"));
            await client.SetWebHook(uri);
        }
    }
}

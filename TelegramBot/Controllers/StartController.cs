﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using TelegramBot.TelegramApi;

namespace TelegramBot.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StartController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly TelegramWebhookTokenStore telegramWebhookTokenStore;

        public StartController(IConfiguration configuration,
            TelegramWebhookTokenStore telegramWebhookTokenStore)
        {
            this.configuration = configuration;
            this.telegramWebhookTokenStore = telegramWebhookTokenStore;
        }

        [HttpGet]
        public IActionResult Get()
        {
            string token = "redacted";

#if DEBUG
            if (Environment.UserInteractive)
            {
                // Expose token in debug mode only
                token = telegramWebhookTokenStore.GetToken();
            }
#endif

            var result = new
            {
                PipedreamEnabled = configuration.GetValue<string>("webhook:pipedream:enabled"),
                TelegramWebhook = Constants.TelegramIncomingWebHookUriWithToken,
                Token = token,
                RenewToken = "/api/Token/Renew",
            };

            return new JsonResult(result);
        }
    }
}

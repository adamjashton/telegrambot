﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace TelegramBot.TelegramApi
{
    public class Client : IDisposable
    {
        // https://api.telegram.org/bot<token>/METHOD_NAME

        private readonly string apiAccessToken;
        private readonly HttpClient httpClient;
        private readonly ILogger<Client> logger;

        public Client(IConfiguration configuration,
            ILogger<Client> logger)
        {
            var baseUrl = configuration.GetSection("telegram:endpoint").Value;
            apiAccessToken = configuration.GetSection("telegram:token").Value;
            this.logger = logger;
            httpClient = new HttpClient()
            {
                BaseAddress = new Uri(baseUrl)
            };            
        }

        public void Dispose()
        {
            httpClient?.Dispose();
        }

        public async Task SetWebHook(Uri address)
        {
            string method = GetFullMethoName("setWebHook");
            var content = new SetWebhook
            {
                Url = address.ToString()
            };
            JsonContent jsonContent = JsonContent.Create(content);
            logger.LogInformation($"Telegram API Client calling '{method}' with content: {await jsonContent.ReadAsStringAsync()}");
            
            var result = await httpClient.PostAsync(method, jsonContent);
            if (!result.IsSuccessStatusCode)
            {
                logger.LogError($"Error response from Telegram API: {await result.Content.ReadAsStringAsync()}");
            }
        }

        public async Task SendMessage(long chatId, string message)
        {
            string method = GetFullMethoName("sendMessage");
            var content = new
            {
                chat_id = chatId,
                text = message
            };
            JsonContent jsonContent = JsonContent.Create(content);
            logger.LogInformation($"Telegram API Client calling '{method}' with content: {await jsonContent.ReadAsStringAsync()}");
            var result = await httpClient.PostAsync(method, jsonContent);
            if (!result.IsSuccessStatusCode)
            {
                logger.LogError($"Error response from Telegram API: {await result.Content.ReadAsStringAsync()}");
            }
        }

        public async Task SendPhoto(long chatId, Uri imageUrl)
        {
            string method = GetFullMethoName("sendPhoto");
            var content = new
            {
                chat_id = chatId,
                photo = imageUrl.ToString()
            };
            JsonContent jsonContent = JsonContent.Create(content);
            logger.LogInformation($"Telegram API Client calling '{method}' with content: {await jsonContent.ReadAsStringAsync()}");
            var result = await httpClient.PostAsync(method, jsonContent);
            if (!result.IsSuccessStatusCode)
            {
                logger.LogError($"Error response from Telegram API: {await result.Content.ReadAsStringAsync()}");
            }
        }

        private string GetFullMethoName(string telegramMethod) => $"/bot{this.apiAccessToken}/{telegramMethod}";
    }
}
